Discord jest programem pozwalającym na transmisję multimedialną pomiędzy użytkownikami.
Członkowie wspólnoty mogą wymieniać się plikami, tekstem lub strumieniami audio/video.

# Uzyskanie dostępu do serwera

## Rejestracja

Do korzystania z serwera konieczna jest [rejestracja](https://discord.com/register). 
Na maila nie będzie przychodził spam.

## Logowanie bez instalacji

Można używać aplikację bez instalacji [poprzez przeglądarkę](https://discord.com/login).
Znane są problemy z konfiguracją dźwięku, dlatego lepiej zainstaliwać.

## Instalacja

Zdecydowanie lepiej jest [pobrać i zainstalować](https://discord.com/download).

# Korzystanie z aplikacji

![wygląd aplikacji](1.png)

1. Lista serwerów.
2. Kanał głosowy – GŁÓWNY. Podczas spoktań właśnie z niego korzystamy.
3. Kanały tekstowe dla spotkań: 
    - ogłoszenia
    - Fausti
    - kanał techniczny *czy mnie słychać?*
4. Pozostałe kanały, które mogą być wykorzystywane poza spotkaniami.
5. Nazwa wybranego kanału [tekstowego].
6. Członkowie wspólnoty.
7. Przycisk wyłączenia mikrofonu
8. Przycisk wyłączenia dźwięku.
9. Ustawienia.

## Spotkania

Aby uniknąć problemów technicznych należy ograniczać sytuacje, w których kilka osób mówi w jednym czasie.
Dlatego wszyscy, poza wyznaczonymi osobami powinni mieć [czasowo] wyłączone mikrofony.
Szczególnie jest to ważne podczas modlitwy. Dobrze wyznaczyć osobę prowadzącą, która będzie ją wypowiadać na głos.

Po wejściu na kanał głosowy wygląd aplikacji nieznacznie się zmienia
![wygląd aplikacji podczas rozmowy](2.png)

1. Osoby na kanale głosowym, mówiący mają zieloną obwódkę. Po kliknięciu PPM można dostosować głośność członka wspólnoty.
2. Przycisk **rozłącz z kanału głosowego**.
3. Strumieniowanie multimediów wideo (kamera/obraz pulpitu)
4. Cały czas można wchodzić na różne kanały tekstowe.